import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ChatClient } from '@azure/communication-chat';
import { AzureCommunicationTokenCredential } from '@azure/communication-common';
import { Observable } from 'rxjs';
import { Influencer } from '../components/influencer-card/influencer-card.component';
import { AuthService } from './auth.service';

// Your unique Azure Communication service endpoint
let endpointUrl = 'https://chat-service.communication.azure.com';

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  private serverHost = 'http://localhost:4000';

  constructor(private auth: AuthService, private http: HttpClient) {
  }

  getContacts(): Observable<Array<Influencer>> {

    return this.http.get<Array<Influencer>>(this.serverHost + '/chat/contacts');
  }

  getToken(): Observable<string> {
    return this.http.get(this.serverHost + '/chat/token', {
      responseType: 'text',
    });
  }

  getCommunicationUserId(user_id: string): Observable<string> {
    return this.http.get(this.serverHost + '/chat/identity/' + user_id, {
      responseType: 'text',
    });
  }

  async createClient(): Promise<ChatClient> {
    
    let userAccessToken = await this.getToken().toPromise();
    let chatClient = new ChatClient(
      endpointUrl,
      new AzureCommunicationTokenCredential(userAccessToken!)
    );
    console.log('Azure communication chat client created!');
    return chatClient;
  }

  public chatClient: ChatClient | undefined;


  async initiateClient() {
    if (this.chatClient == undefined) {
      this.chatClient = await this.createClient();
    }
  }

  async createChatThread() {
    const createChatThreadRequest = {
      topic: 'Hello, World!',
    };

    const username = this.auth.getUsername() || 'could not find username';
    const user_id = this.auth.getUser_id() || 'could not find user id';
    let communicationUserId =
      (await this.getCommunicationUserId(user_id).toPromise()) || ' ';

    const createChatThreadOptions = {
      participants: [
        {
          id: { communicationUserId: communicationUserId },
          displayName: username,
        },
      ],
    };

    let chatClient = await this.chatClient;

    if (chatClient) {
      const createChatThreadResult = await chatClient.createChatThread(
        createChatThreadRequest,
        createChatThreadOptions
      );

      if (createChatThreadResult.chatThread) {
        console.log('Chat thread created!');
        const threadId = createChatThreadResult.chatThread.id;
        return threadId;
      }
      return '-1';
    }
    return '-1';
  }

  fetchThreadId(user_id: string): Observable<threadIdResponse> {
    return this.http.get<threadIdResponse>(
      this.serverHost + '/chat/thread/' + user_id
    );
  }

  storeThreadId(user_id: string, threadId: string) {
    console.log('storing thread id...');
    return this.http.post(this.serverHost + '/chat/thread/' + user_id, {
      threadId: threadId,
    });
  }
}

interface threadIdResponse {
  threadId: string;
  userId1: string;
  userId2: string;
}
