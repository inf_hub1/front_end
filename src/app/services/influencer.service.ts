import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InfluencerService {
  private url = "http://localhost:4000/influencers/";

  constructor(private http: HttpClient) { }
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json'}),
  };

  public becomeInfluencer(data:any): Observable<any> {
    return this.http.post(this.url,data,this.httpOptions);
  }
  public getinfuencers(categories:any):Observable<any>{
    let query="";
    if(categories.length!=0) query="?categories[]="+categories.join("&categories[]=");
    console.log(this.url+query);
    return this.http.get(this.url+query,this.httpOptions);
  }
}
