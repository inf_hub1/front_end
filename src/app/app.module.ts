import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import { ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './pages/home/home.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import {MatChipsModule} from '@angular/material/chips';
import { RegisterComponent } from './components/register/register.component';
import { DiscoverComponent } from './pages/discover/discover.component';
import { FooterComponent } from './footer/footer.component';
import { InfluencerCardComponent } from './components/influencer-card/influencer-card.component'; 
import {MatDividerModule} from '@angular/material/divider';
import { InboxComponent } from './pages/inbox/inbox.component';
import { ContactComponent } from './components/contact/contact.component'; 
import { ScrollingModule } from '@angular/cdk/scrolling';
import { ChatComponent } from './components/chat/chat.component';
import {MatTabsModule} from '@angular/material/tabs'; 
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './auth.interceptor';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

import { BecomeInfluencerComponent } from './components/become-influencer/become-influencer.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    DiscoverComponent,
    FooterComponent,
    InfluencerCardComponent,
    InboxComponent,
    ContactComponent,
    ChatComponent,
    BecomeInfluencerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatListModule,
    MatSidenavModule,
    MatIconModule,
    MatDialogModule,
    MatChipsModule,
    MatDividerModule,
    ScrollingModule,
    HttpClientModule,
    MatTabsModule,
    MatProgressSpinnerModule
    /*NbChatModule,
    NbThemeModule,
    NbLayoutModule,
    NbSidebarModule,
    NbButtonModule,
    NbMenuModule,
    NbThemeModule.forRoot(),
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    */
  ],
  providers: [
    /*NbSidebarService,
    NbStatusService,
    NbFocusMonitor,
    NbThemeService,*/
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
