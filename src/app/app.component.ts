import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BecomeInfluencerComponent } from './components/become-influencer/become-influencer.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'inf_hub_font_end';
  Dialog_isopen=false;


  constructor (public auth: AuthService,private dialog: MatDialog, private router:Router) {}
  
  public myid = this.auth.getUser_id();
  
  openDialog(): void {
    if(!this.Dialog_isopen){
      this.Dialog_isopen=true;
      const dialogRef = this.dialog.open(LoginComponent, {
       
      });
      dialogRef.afterClosed().subscribe(result => {
        this.Dialog_isopen=false;
      });
    }
  }
  openRegisterDialog(): void {
    if(!this.Dialog_isopen){
      this.Dialog_isopen=true;
      const dialogRef = this.dialog.open(RegisterComponent, {
        
      });
  
      dialogRef.afterClosed().subscribe(result => {
        this.Dialog_isopen=false;
      });

    }
    
  }
  openbecomeAdDialog(): void {
    if(!this.Dialog_isopen){
      this.Dialog_isopen=true;
      const dialogRef = this.dialog.open( BecomeInfluencerComponent, {
        height: '800px',
        width: '600px',
      });
      dialogRef.afterClosed().subscribe(result => {
        this.Dialog_isopen=false;
      });

    }
    
  }

  goToDiscover() {
    this.router.navigateByUrl("/discover")
  }


}
