import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Influencer } from 'src/app/components/influencer-card/influencer-card.component';
import { AuthService } from 'src/app/services/auth.service';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit {

  constructor(private chatService: ChatService, private auth: AuthService, private route: ActivatedRoute) { }

  async ngOnInit() {
    const user_id = this.route.snapshot.paramMap.get("influencer_id") || " ";
    await this.chatService.getContacts().toPromise().then(contacts => {
      this.influencers = contacts || [];
    });
    let username = "test";
    await this.auth.getUsernameFromId(user_id).subscribe(value => console.log(value));
    let firstTime = true;
    this.influencers.forEach(influencer=> {
      if(influencer.influence_id == user_id) {
        firstTime = false
      }
    });
    if(firstTime && this.auth.getUser_id()!=user_id){
      this.influencers.push({
        influence_id: user_id,
        username: username,
        profile_picture:"",
        number_followers:"",
        categories:[],
        instagram_url:"",
        youtube_url:""
      });
    }
    
    console.log(this.influencers);
  }

  influencers: Influencer[] = [];
  curr_contact: Influencer | undefined;
  contact_id:string| undefined
  get_conversation(contact: any) {
    this.curr_contact = contact;
    this.contact_id=contact.influence_id;
  }

}
