import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { Influencer } from 'src/app/components/influencer-card/influencer-card.component';
import { InfluencerService } from 'src/app/services/influencer.service';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.scss'],
})
export class DiscoverComponent implements OnInit {
  constructor(private influencerService: InfluencerService) {}

  influencers: Influencer[] = [];

  ngOnInit(): void {
    this.getInfuencers([]);
  }
  getInfuencers(categories: any) {
    this.influencerService
      .getinfuencers(categories)
      .subscribe((influencers: any) => {
        this.influencers = influencers;
        this.influencers = influencers.map((influencer:any) => {
          influencer.influence_id = influencer._id;
          return influencer;
        });
        console.log(influencers);
      });
  }
  categories = [
    'all categories',
    'beauty',
    'fitness',
    'art',
    'music',
    'food',
    'gaming',
    'shopping',
    'nature',
    'blog',
    'health',
    'sport',
    'Medical and Health',
    'Beauty, Cosmetic and Personal Care',
    'Travel and Transportation',
    'Writing',
    'gaming2',
    'shopping2',
    'nature2',
    'blog2',
    'health2',
    'sport2',
    'Medical and Health2',
    'Beauty, Cosmetic and Personal Care2',
    'Travel and Transportation2',
    'Writing2',
  ];
  keywords = new Set(this.categories);
  formControl = new FormControl(['all categories']);
  addKeyword(keyword: string) {
    let ind = this.formControl.value.indexOf(keyword);
    if (ind == -1) {
      this.formControl.value.push(keyword);
      this.formControl = new FormControl(this.formControl.value);
    } else {
      this.formControl.value.splice(ind, 1);
      this.formControl = new FormControl(this.formControl.value);
    }
    this.getInfuencers(this.formControl.value.slice(1));

    
  }
}
