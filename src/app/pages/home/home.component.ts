import { Component, OnInit } from '@angular/core';
import { Influencer } from 'src/app/components/influencer-card/influencer-card.component';
import { InfluencerService } from 'src/app/services/influencer.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private influencerService: InfluencerService) { }
  getInfuencers(categories: any) {
    this.influencerService
      .getinfuencers(categories)
      .subscribe((influencers: any) => {
        this.influencers = influencers.map((influencer:any) => {
          influencer.influence_id = influencer._id;
          return influencer;
        });
        this.influencers=this.influencers.slice(0,5);
      });
  }
  ngOnInit(): void {
    this.getInfuencers([]);
  
  }
  influencers:Influencer[]=[];
}
