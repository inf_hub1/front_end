import { HttpClient } from '@angular/common/http';
import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { InfluencerService } from 'src/app/services/influencer.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes';

@Component({
  selector: 'app-become-influencer',
  templateUrl: './become-influencer.component.html',
  styleUrls: ['./become-influencer.component.scss'],
})
export class BecomeInfluencerComponent implements OnInit {
  fileName = '';

  fileHandlerUrl = 'http://localhost:4000/file';

  constructor(
    private http: HttpClient,
    public auth: AuthService,
    private router: Router,
    private influencerService: InfluencerService,
    private injector: Injector,
    public dialogRef: MatDialogRef<BecomeInfluencerComponent>
  ) {}
  loginSuccess = false;
  InfluencerService: any;

  ngOnInit(): void {}
  form: FormGroup = new FormGroup({
    instagram_username: new FormControl('')
    
  });
  form_urls: FormGroup = new FormGroup({
    instagram_url: new FormControl(''),
    youtub_url: new FormControl('')
  });

  file: any = null;
  onFileSelected(event: Event) {
    this.file = (event.target as HTMLInputElement).files![0];

    if (this.file) {
      this.fileName = this.file.name;
    }
  }

  submit() {
    this.become_influencer();
  }

 

  async become_influencer(): Promise<void> {
    console.log(this.categories);

    if(this.file && this.form.value.instagram_username && this.categories.length!=0){
      const formData = new FormData();
      formData.append('avatar', this.file);
      console.log(this.form_urls.value.instagram_url,this.form_urls.value.youtub_url)
      const upload$ = await this.http.post(this.fileHandlerUrl, formData);
      upload$.subscribe();
      this.influencerService
        .becomeInfluencer({
          user_name :this.form.value.instagram_username,
          youtub_url :this.form_urls.value.youtub_url,
          instagram_url:this.form_urls.value.instagram_url,
          categories: this.categories
        })
        .subscribe((e: any) => {
          this.dialogRef.close('');
        });
    }
   
  }
  @Input() error: string | null | undefined;

  @Output() submitEM = new EventEmitter();

  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  categories: String[] = [];

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.categories.push(value);
    }

    // Clear the input value
    event.chipInput!.clear();
  }

  remove(categorie: any): void {
    const index = this.categories.indexOf(categorie);

    if (index >= 0) {
      this.categories.splice(index, 1);
    }
  }
}
