import { Component, Input, OnInit } from '@angular/core';
export interface Influencer{
  influence_id:string;
  username:string;
  profile_picture:string;
  number_followers:string;
  categories: string[];
  instagram_url:string;
  youtube_url:string;
}

@Component({
  selector: 'app-influencer-card',
  templateUrl: './influencer-card.component.html',
  styleUrls: ['./influencer-card.component.scss']
})
export class InfluencerCardComponent implements OnInit {

  @Input() influencer:Influencer | undefined;

  constructor() { }

  ngOnInit(): void {
  }

}
