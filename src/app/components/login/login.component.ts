import {  Injector, OnInit } from '@angular/core';
import { Input, Component, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { AuthService } from 'src/app/services/auth.service';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginSuccess = false;

  constructor(private chatService: ChatService,private injector: Injector,public dialogRef: MatDialogRef<LoginComponent>) { }

  ngOnInit(): void {
  }
  form: FormGroup = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });

  submit() {
    let auth = this.injector.get(AuthService);
    if (this.form.valid) {
      auth.login(this.form.value.email, this.form.value.password).subscribe({next: async authResult => {
        auth.setSession(authResult);
        this.loginSuccess = true;
        await this.chatService.initiateClient();
        this.dialogRef.close('');
      },
      error: error => {
        this.loginSuccess = false;
      }})
      if(this.loginSuccess){
        this.submitEM.emit(this.form.value);
      }  
    }
  }
  @Input() error: string | null | undefined;

  @Output() submitEM = new EventEmitter();

}
