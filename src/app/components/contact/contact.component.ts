import { Component, Input, OnInit } from '@angular/core';
import { Influencer } from '../influencer-card/influencer-card.component';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  constructor() { }

  @Input() contact:Influencer | undefined;


  ngOnInit(): void {
    
  }

}
