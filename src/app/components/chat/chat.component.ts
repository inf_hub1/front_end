import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import {
  ChatClient,
  ChatMessage,
  ChatMessageType,
  ChatThreadClient,
} from '@azure/communication-chat';
import { AuthService } from 'src/app/services/auth.service';
import { ChatService } from 'src/app/services/chat.service';
import { Influencer } from '../influencer-card/influencer-card.component';

export interface Message {
  msg_text: string;
  from_name: string;
  from_id: string;
  to_name: string;
  to_id: string;
  date: string;
}

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit {
  @Input() contact: Influencer | undefined;
  @Input() contact_id: string | undefined;

  

  public messageContent = new FormControl('');

  constructor(private chatService: ChatService, private auth: AuthService) {

   }

  

  ngOnChanges(changes: SimpleChanges) {
    console.log(this.contact?.influence_id);
    if (changes['contact_id']) {
      this.messages = [];
      this.stillLoading = true;
      this.ngOnInit()
    }
  }

  messages:ChatMessage[]=[];
  myid:string="1"

  chatThreadClient: ChatThreadClient | undefined;
  chatClient: ChatClient | undefined;

  stillLoading = true;

  async ngOnInit() {

    this.contact_id= this.contact?.influence_id;

    await this.chatService.initiateClient();

    this.myid = (await this.auth.getUsername()) || ' ';

    if (this.contact) {
      let firstTime = false;
      var threadId: string | null = null;
      await this.chatService
        .fetchThreadId(this.contact.influence_id)
        .toPromise()
        .then((value) => {
          if (value) {
            threadId = value.threadId;
          } else {
            threadId = null;
          }
        });
      console.log('fetched thread' + threadId);
      if (!threadId) {
        firstTime = true;
        threadId = await this.createThread();
        this.chatService
          .storeThreadId(this.contact.influence_id, threadId)
          .toPromise();
      }
      this.chatClient = await this.getChatClient();
      this.chatThreadClient = await this.chatClient?.getChatThreadClient(
        threadId
      );
      let communicationUserId =
        (await this.chatService
          .getCommunicationUserId(this.contact.influence_id)
          .toPromise()) || '  ';
      if (firstTime) {
        await this.addParticipant(
          this.chatThreadClient!,
          communicationUserId,
          this.contact.username
        );
      }
      await this.retrieveMessages(this.chatThreadClient!);
      this.stillLoading = false;
      await this.chatClient?.startRealtimeNotifications();
      // subscribe to new notification
      this.chatClient?.on('chatMessageReceived', (e) => {
        console.log('Notification chatMessageReceived!');
        this.retrieveMessages(this.chatThreadClient!);
      });
    }
  }

  async createThread(): Promise<string> {
    const threadId = await this.chatService.createChatThread();
    console.log(`Thread created:${threadId}`);
    return threadId;
  }

  async sendMessage(/*content: string, */ chatThreadClient: ChatThreadClient) {
    if (this.messageContent.value) {
      const sendMessageRequest = {
        content: this.messageContent.value,
      };
      let sendMessageOptions = {
        senderDisplayName:
          (await this.auth.getUsername()) || "Couldn't retrieve username",
        type: <ChatMessageType>'text',
      };
      const sendChatMessageResult = await chatThreadClient.sendMessage(
        sendMessageRequest,
        sendMessageOptions
      );
      const messageId = sendChatMessageResult.id;
      console.log(`Message sent! message id:${messageId}`);
      this.messages.unshift({
        id:"placeholder", 
        type: "text",
        sequenceId:"sequenceId",
        version:"version",
        createdOn: new Date(),
        senderDisplayName: await this.auth.getUsername()!,
        content: {
          message: this.messageContent.value
        }
      })
      this.retrieveMessages(chatThreadClient);
      this.messageContent.reset(this.messageContent.value);
      return messageId;
    }
    return null;
  }

  async retrieveMessages(chatThreadClient: ChatThreadClient) {
    const messages = await chatThreadClient.listMessages();
    let temp = [];
    for await (const message of messages) {
      if (message.type == 'text') {
        temp.push(message);
      }
    }
    //temp.reverse();
    this.messages = temp;
  }

  async addParticipant(
    chatThreadClient: ChatThreadClient,
    communicationUserId: string,
    displayName: string
  ) {
    const addParticipantsRequest = {
      participants: [
        {
          id: { communicationUserId: communicationUserId },
          displayName: displayName,
        },
      ],
    };

    await chatThreadClient.addParticipants(addParticipantsRequest);
    console.log('added participant');
  }

  async getChatClient() {
    const chatClient = await this.chatService.chatClient;

    return chatClient;
  }
}
